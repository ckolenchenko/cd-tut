var request = require('supertest');
var app = require('../app.js')

describe('GET /', function() {
  it('displays "Hello World-2!"', function(done) {
    // The line below is the core test of our app.
    request(app)
      .get('/')
      .expect(function(res) {
        if (res.text.indexOf('Hello World-2!') == -1) throw new Error ("Missing Hello World-2!");
      })
      .expect(200, done);
  });
});