const __CONFIG = require(__dirname+'/config/settings.json');
const express = require('express');
const app = express();

// The code below will display 'Hello World How is it going!' to the browser when you go to http://localhost:3000
app.get('/', function (req, res) {
  res.send('Hello World-2!') ;
});

var port = process.env.PORT || __CONFIG.port;

app.listen(port, function () {
  console.log('Example app listening on port '+__CONFIG.port+'!');
});

module.exports = app;